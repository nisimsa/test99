<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UrlManager\Url;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->homeUrl = ['site/index'];	
		Yii::$app->user->logout();

		return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
	
	
	////////////q4/////////////////
	
			public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$viewBonus = $auth->createPermission('viewBonus');
		$viewBonus->description = 'View Bonuses';
		$auth->add($viewBonus);
		
		$indexBonus = $auth->createPermission('indexBonus');
		$indexBonus->description = 'Not all users can view Bonus';
		$auth->add($indexLeads);
		
	}
	
		public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createBonus = $auth->createPermission('createBonus');
		$createBonus->description = 'Team Admin can create new Bonuses';
		$auth->add($createBonus);
		
		$updateBonus = $auth->createPermission('updateBonus');
		$updateBonus->description = 'Team Admin can update
									Bonuses including assignment';
		$auth->add($updateBonus);
	}
	
	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teamleader = $auth->getRole('teamleader');
	
		$indexBonus = $auth->getPermission('indexBonus');
		$auth->addChild($teamleader, $indexBonus);	

		$admin = $auth->getRole('admin');
		
		$createBonus = $auth->getPermission('createBonus');
		$auth->addChild($admin, $createBonus);

		$updateBonus = $auth->getPermission('updateBonus');
		$auth->addChild($admin, $updateBonus);
	}

	
	
	//////////finishq4/////////////////////
}
